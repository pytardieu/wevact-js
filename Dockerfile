# base image
FROM node:12.17.0-alpine as build-stage

# set working directory
WORKDIR /app

# add `/app/node_modules/.bin` to $PATH
ENV PATH /app/node_modules/.bin:$PATH

# install and cache app dependencies

COPY package.json /app/package.json
RUN npm install
RUN npm install react-scripts@3.0.1 -g --silent

# start app
COPY . /app
RUN npm run build --prod

FROM nginx:stable-alpine as production-stage
RUN mkdir /app

COPY --from=build-stage /app/build /app
COPY ./nginx.conf /etc/nginx/conf.d/default.conf

CMD nginx -g 'daemon off;'
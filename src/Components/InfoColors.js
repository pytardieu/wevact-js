import React from "react";
import Paper from "@material-ui/core/Paper";
import {Typography} from "@material-ui/core";
import FiberManualRecordIcon from "@material-ui/icons/FiberManualRecord";

function InfoColors() {
    const colors = [
        {
            'color': 'noire',
            'description': '15 modules variés permettant de rencontrer des services de l\'INSA ou des partenaires extérieurs.',
            'code': '#000000'
        },
        {
            'color': 'verte',
            'description': '11 modules reprenant les bases de la gestion associative (de la Tréso à la gestion d\'équipes…)',
            'code': '#009933'
        },
        {
            'color': 'bleue',
            'description': '12 modules pour approfondir ses connaissances en gestion associative (de la suite Adobe à la rédaction de statuts en passant par la RGPD…)',
            'code': '#3333ff'
        },
        {
            'color': 'jaune',
            'description': '10 modules pour discuter de thématiques associatives avec d\'autres étudiant.e.s (ou ancien.ne.s)',
            'code': '#ffff00'
        }
    ]
    return (
        <Paper elevation={3} style={{marginTop: '10px'}}>
            {
                colors.map((color) => {
                    return (
                        <div style={{padding: '20px'}}>
                            <Typography variant="h6">
                                <FiberManualRecordIcon style={{color: color.code}} />
                                Couleur {color.color}
                                <FiberManualRecordIcon style={{color: color.code}} />
                            </Typography>
                            <Typography variant="body1">{color.description}</Typography>
                        </div>
                    )})
            }
        </Paper>
    );
}

export default InfoColors;

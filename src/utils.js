import config from "./config.json"

export default function getDomain(){
    return config.DEBUG ? config.URL_DEV : config.URL_PROD
};

import React from 'react';
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import Planning from "./View/Planning";
import Reservation from "./View/Reservation";
import Home from "./View/Home";
import Dashboard from "./View/Dashboard"
import Success from "./View/Success";

import './App.css';
import Footer from "./Components/Footer";


function App() {

  return (
      <div className="App">
          <Router>
              <Switch>
                  <Route path="/reservation">
                      <Reservation />
                  </Route>
                  <Route path="/planning">
                      <Planning />
                  </Route>
                  <Route path="/dashboard">
                      <Dashboard />
                  </Route>
                  <Route path="/success">
                      <Success />
                  </Route>
                  <Route path="/">
                      <Home />
                  </Route>
              </Switch>
          </Router>

          <Footer/>
      </div>
  );
}

export default App;

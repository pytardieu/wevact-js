import React, {useEffect, useState} from "react";
import axios from "axios";
import getDomain from "../utils";
import Module from "./Module";
import FormLabel from "@material-ui/core/FormLabel";
import FormControl from "@material-ui/core/FormControl";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import RadioGroup from "@material-ui/core/RadioGroup";
import Radio from "@material-ui/core/Radio";
import {makeStyles} from "@material-ui/core/styles";
import Accordion from "@material-ui/core/Accordion";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import Typography from "@material-ui/core/Typography";
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

const useStyles = makeStyles((theme) => ({
    formUnit: {
        margin: theme.spacing(1),
        padding: theme.spacing(1),
    },
}));

function ModuleList({selected=[], modif}) {
    const classes = useStyles()
    const days = [
        {
            'letter': 'V',
            'descr': 'Vendredi 9/10 en Gustave Ferrier (GE)'
        },
        {
            'letter': 'S',
            'descr': 'Samedi 10/10 en Pierre de Fermat (FIMI)'
        },
        {
            'letter': 'D',
            'descr': 'Dimanche 11 en Pierre de Fermat (FIMI)'
        }
    ]

    const [moduleList, setModuleList] = useState([]);
    const [creneauList, setCreneauList] = useState([])
    const [modulesSelect, setModulesSelect] = useState([])
    const [creneauxPk, setCreneauxPk] = useState([])

    useEffect(() => {
        axios
            .get(getDomain() + "/api/modules/")
            .then(modules => {
                axios
                    .get(getDomain() + '/api/creneau/')
                    .then(creneaux => {
                        console.log(creneaux.data);
                        console.log(modules.data);

                        let selectedRadio = {};

                        for (let i=0;i<selected.length;i++) {
                            for (let j=0;j<modules.data.length;j++) {
                                if (modules.data[j].pk === selected[i]) {
                                    selectedRadio[modules.data[j].creneau] = String(selected[i]);
                                    break;
                                }
                            }
                        }

                        console.log(selectedRadio);

                        let CrenalList = {};
                        for (let i=0;i<creneaux.data.length;i++) {
                            CrenalList[creneaux.data[i].pk] = creneaux.data[i];
                        }
                        console.log(CrenalList);

                        let mod = {};
                        let creneauxPkinit = [];
                        for (let j = 0;j<creneaux.data.length;j++) {
                            mod[creneaux.data[j].pk] = [];
                            creneauxPkinit.push(creneaux.data[j].pk)
                        }

                        console.log(creneauxPkinit);

                        for (let i = 0; i < modules.data.length; i++) {
                            mod[modules.data[i].creneau].push(modules.data[i])
                        }

                        console.log(mod);

                        setModuleList(mod);
                        setModulesSelect(selectedRadio);
                        setCreneauList(CrenalList);
                        setCreneauxPk(creneauxPkinit);


                    })
                    .catch(err => {
                        console.log(err);
                    });
            })
            .catch(err => console.log(err));

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <div>
            {days.map((day) => {
                return (
                    <Accordion style={{margin: 20}}>
                        <AccordionSummary
                            expandIcon={<ExpandMoreIcon />}
                            aria-controls="panel1a-content"
                            id="panel1a-header"
                        >
                            <Typography variant="h6">{day.descr}</Typography>
                        </AccordionSummary>
                        <AccordionDetails>
                            <div>
                            {creneauxPk.map((creneau) => {
                                return (creneauList[creneau].inscription_open &&
                                        creneauList[creneau].formated[0] === day.letter && // Creneau corresponds to this accordion
                                    <div className={classes.formUnit}>
                                        <FormControl>
                                            <FormLabel>{creneauList[creneau].formated}</FormLabel>
                                            <RadioGroup className={classes.formUnit} aria-label={""} name={"creneau" + String(creneau)} defaultValue={modulesSelect[creneau]}>
                                                {
                                                    moduleList[creneau].map((module) => {
                                                        return (
                                                            <Module module={module} modif={modif}/>
                                                        );})
                                                }
                                                <FormControlLabel value="null" control={<Radio />} label="Aucun" disabled={!modif}/>

                                            </RadioGroup>
                                        </FormControl>
                                    </div>
                                )
                            })}
                            </div>
                        </AccordionDetails>
                    </Accordion>
                );
            })}
        </div>
    )
}


export default ModuleList;

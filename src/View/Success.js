import React from 'react';
import {Button, Container, Typography} from "@material-ui/core";
import { makeStyles } from '@material-ui/core/styles';
import Alert from "@material-ui/lab/Alert";
//import Banniere from "../Assets/WEVA.png";

const useStyles = makeStyles((theme) => ({
    root: {
        '& > *': {
            margin: theme.spacing(1),
        },
    },
}));

function Success () {
    const classes = useStyles();

    return (
        <React.Fragment>

            <Typography variant="h1">WEVA</Typography>
            <Typography variant="h3">Du 9 au 11 Octobre</Typography>

            <Container maxWidth='xl' className={classes.root}>
                <Alert severity="success">Inscription terminée avec succès !</Alert>
                <Button variant="contained" color="primary" href='/reservation'>S'inscrire</Button>
                <Button variant="contained" color="secondary" href='/planning'>Modifier mon inscription</Button>
                <Button variant="contained" href='/dashboard'>Places disponibles</Button>
            </Container>
        </React.Fragment>
    );

    /*

                /*<Grid item md={10}>
                <img src={Banniere} alt="Bannière WEVA" sm={8} width="100%"/>
            </Grid>
     */
}

export default Success;
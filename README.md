# WEVAct-JS

Front-end de WEVAmaker, pour l'organisation et l'inscription aux modules du WEVA

## Install
```bash
npm install
npm run start
```

## Install w/ Docker
```bash
docker build -t wevact:1 .
docker run -it --rm -v ${PWD}:/app -p 3000:3000 wevact:1
```